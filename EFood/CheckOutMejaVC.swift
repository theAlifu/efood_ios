import UIKit

class CheckOutMejaVC: UIViewController {
    
    @IBOutlet var headCheck: HeaderApp!
    @IBOutlet var harga: UILabel!
    @IBOutlet var ship: FoodText!
    @IBOutlet var meja: FoodText!
    @IBOutlet var btnCheck: UIButton!
    
    var dtHarga: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.headCheck.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }

}
