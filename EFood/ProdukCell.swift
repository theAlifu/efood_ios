import UIKit

class ProdukCell: UICollectionViewCell {
    
    @IBOutlet var img: CustomImageView!
    @IBOutlet var hrg: UILabel!
    @IBOutlet var btnMore: UIButton!
    @IBOutlet var nama: UILabel!
    @IBOutlet var kategori: UILabel!
    @IBOutlet var lebarView: NSLayoutConstraint!
    
    let screenWidth = UIScreen.main.bounds.size.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        lebarView.constant = screenWidth
    }

}
