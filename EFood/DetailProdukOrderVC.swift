import UIKit

class DetailProdukOrderVC: UIViewController, URLSessionDelegate {

    @IBOutlet var headerDetail: HeaderApp!
    @IBOutlet var img: CustomImageView!
    @IBOutlet var nama: UILabel!
    @IBOutlet var kategori: UILabel!
    @IBOutlet var Harga: UILabel!
    @IBOutlet var keterangan: UILabel!
    @IBOutlet var btnPesan: UIButton!
    @IBOutlet var jml: FoodText!
    
    var dtId: String = ""
    var dtImg: String = ""
    var dtNama: String = ""
    var dtKategori: String = ""
    var dtHarga: String = ""
    var dtSatuan: String = ""
    var dtKeterangan: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerDetail.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.img.loadImageUsingUrlString(self.dtImg, kata: "")
        self.nama.text = self.dtNama
        self.kategori.text = self.dtKategori
        self.Harga.text = "Rp." + self.dtHarga
        self.keterangan.text = self.dtKeterangan
        
        self.btnPesan.addTarget(self, action: #selector(self.aksiPesan), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func aksiPesan() {
        self.panggilPesan()
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilPesan()
    }
    
    func panggilPesan() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriCartPesan)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let body = "produk_id=\(self.dtId)&nama_produk=\(self.dtNama)&harga=\(self.dtHarga)&satuan=\(self.dtSatuan)&jumlah=\(self.jml.isi.text!)&disc=0"
        request.httpBody = body.data(using: .utf8)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Sukses Memesan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }
    
}
