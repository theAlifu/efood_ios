import UIKit

class DetailKasirVC: UIViewController {
    
    @IBOutlet var headDetail: HeaderApp!
    @IBOutlet var noTrans: UILabel!
    @IBOutlet var tglTrans: UILabel!
    @IBOutlet var jumlahBayar: UILabel!
    @IBOutlet var Ship: UILabel!
    @IBOutlet var bayarShip: UILabel!
    @IBOutlet var alamat: UILabel!
    @IBOutlet var totalbayar: UILabel!
    @IBOutlet var collectData: UICollectionView!
    @IBOutlet var btnBayar: UIButton!
    
    var arrDapur = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
