import UIKit

class MsaterPengirimanVC: UIViewController, URLSessionDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet var headerKirim: HeaderApp!
    @IBOutlet var collectData: UICollectionView!
    
    var arrPengiriman = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let flowLayout = collectData.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectData.register(UINib(nibName: "PengirimanCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.headerKirim.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.headerKirim.btnDua.addTarget(self, action: #selector(self.panggilForm), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.panggilPengiriman()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func panggilForm() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "FormPengirimanVC") as! FormPengirimanVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPengiriman.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectData.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PengirimanCell
        let data = self.arrPengiriman[indexPath.row]
        cell.ship.text = "\(data["ship"] as! String)"
        cell.harga.text = "\(data["harga"] as! Int)"
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.panggilOpsiList(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func panggilOpsiList(sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Pilih", preferredStyle: .alert)
        let edit = UIAlertAction(title: "Edit", style: .default) { (edit) in
            
        }
        let hapus = UIAlertAction(title: "Hapus", style: .default) { (hapus) in
            
        }
        let batal = UIAlertAction(title: "Batal", style: .cancel) { (hapus) in
            
        }
        alert.addAction(edit)
        alert.addAction(hapus)
        alert.addAction(batal)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilPengiriman()
    }
    
    func panggilPengiriman() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriMasterPengiriman)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrPengiriman = dt as! [[String : AnyObject]]
                }
                self.collectData.reloadData()
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
