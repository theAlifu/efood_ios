import UIKit

struct dtLogin {
    var id: Int = 0
    var name: String = ""
    var email: String = ""
    var telp: String = ""
    var level: String = ""
    var token: String = ""
}

class LoginVC: UIViewController, URLSessionDelegate, UITextFieldDelegate {

    @IBOutlet var viewField: UIView!
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var btnMasuk: UIButton!
    @IBOutlet var btnRegis: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnMasuk.addTarget(self, action: #selector(self.masuk), for: .touchUpInside)
        self.username.delegate = self
        self.password.delegate = self
        self.btnRegis.addTarget(self, action: #selector(self.panggilRegis), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func masuk() {
        self.username.resignFirstResponder()
        self.password.resignFirstResponder()
        if self.username.text! == "" {
            return
        }
        if self.password.text! == "" {
            return
        }
        self.panggilLogin()
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilLogin()
    }
    
    func panggilLogin() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriLogin)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        let body = "email=\(self.username.text!)&password=\(self.password.text!)"
        request.httpBody = body.data(using: .utf8)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                DATA_LOGIN.id = swiftyJsonVar["response"]["message"]["id"].int!
                DATA_LOGIN.name = swiftyJsonVar["response"]["message"]["name"].string!
                DATA_LOGIN.email = swiftyJsonVar["response"]["message"]["email"].string!
                DATA_LOGIN.telp = swiftyJsonVar["response"]["message"]["telp"].string!
                DATA_LOGIN.level = swiftyJsonVar["response"]["message"]["level"].string!
                DATA_LOGIN.token = swiftyJsonVar["response"]["message"]["token"].string!
                self.pilihForm()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

    func pilihForm() {
        closeLoading()
        if DATA_LOGIN.level == "admin" {
            let panggil = self.storyboard?.instantiateViewController(withIdentifier: "AdminDepanTBC") as! AdminDepanTBC
            self.navigationController?.pushViewController(panggil, animated: true)
        } else if DATA_LOGIN.level == "meja" {
            let panggil = self.storyboard?.instantiateViewController(withIdentifier: "MejaDepanTBC") as! MejaDepanTBC
            self.navigationController?.pushViewController(panggil, animated: true)
        } else if DATA_LOGIN.level == "dapur" {
            let panggil = self.storyboard?.instantiateViewController(withIdentifier: "DapurDepanTBC") as! DapurDepanTBC
            self.navigationController?.pushViewController(panggil, animated: true)
        } else if DATA_LOGIN.level == "kasir" {
            let panggil = self.storyboard?.instantiateViewController(withIdentifier: "KasirDepanTBC") as! KasirDepanTBC
            self.navigationController?.pushViewController(panggil, animated: true)
        } else if DATA_LOGIN.level == "pelanggan" {
            let panggil = self.storyboard?.instantiateViewController(withIdentifier: "DepanTBC") as! DepanTBC
            self.navigationController?.pushViewController(panggil, animated: true)
        }
    }
    
    @objc func panggilRegis() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "RegisVC") as! RegisVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
}
