import UIKit

class FeedCell: UICollectionViewCell {
    
    @IBOutlet var lebarView: NSLayoutConstraint!
    @IBOutlet var backImg: CustomImageView!
    @IBOutlet var nama: UILabel!
    @IBOutlet var kategori: UILabel!
    @IBOutlet var hrg: UILabel!
    
    let screenWidth = UIScreen.main.bounds.size.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        lebarView.constant = screenWidth
    }

}
