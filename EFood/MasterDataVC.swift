import UIKit

class MasterDataVC: UIViewController {
    
    @IBOutlet var headerMaster: HeaderApp!
    @IBOutlet var btnMeja: ButtonCenter!
    @IBOutlet var btnKategori: ButtonCenter!
    @IBOutlet var btnProduk: ButtonCenter!
    @IBOutlet var btnPengiriman: ButtonCenter!
    @IBOutlet var btnUser: ButtonCenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnMeja.tapReg.addTarget(self, action: #selector(self.panggilMeja))
        self.btnMeja.addGestureRecognizer(self.btnMeja.tapReg)
        self.btnKategori.tapReg.addTarget(self, action: #selector(self.panggilKategori))
        self.btnKategori.addGestureRecognizer(self.btnKategori.tapReg)
        self.btnProduk.tapReg.addTarget(self, action: #selector(self.panggilProduk))
        self.btnProduk.addGestureRecognizer(self.btnProduk.tapReg)
        self.btnPengiriman.tapReg.addTarget(self, action: #selector(self.panggilPengiriman))
        self.btnPengiriman.addGestureRecognizer(self.btnPengiriman.tapReg)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func panggilMeja() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "MasterMejaVC") as! MasterMejaVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    @objc func panggilKategori() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "MasterKategoriVC") as! MasterKategoriVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    @objc func panggilProduk() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "MasterProductVC") as! MasterProductVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    @objc func panggilPengiriman() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "MsaterPengirimanVC") as! MsaterPengirimanVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }

}
