import UIKit

class FormPengirimanVC: UIViewController, URLSessionDelegate {
    
    @IBOutlet var headerForm: HeaderApp!
    @IBOutlet var Ship: FoodText!
    @IBOutlet var harga: FoodText!
    @IBOutlet var btnSimapn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerForm.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.btnSimapn.addTarget(self, action: #selector(self.aksiSimpan), for: .touchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func aksiSimpan() {
        self.prosesSimpan()
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.prosesSimpan()
    }
    
    func prosesSimpan() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriMasterPengirimanSimpan)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let body = "ship=\(self.Ship.isi.text!)&harga=\(self.harga.isi.text!)"
        request.httpBody = body.data(using: .utf8)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Tersimpan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
            } else if status == 401 {
                let msgship = swiftyJsonVar["response"]["message"]["ship"].string
                let msgharga = swiftyJsonVar["response"]["message"]["harga"].string
                if msgship == nil {
                    stopLoading(untukBtn: "Tutup", untukPesan: msgship!)
                } else {
                    stopLoading(untukBtn: "Tutup", untukPesan: msgharga!)
                }
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
