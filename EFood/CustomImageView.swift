//
//  CustomImageView.swift
//  Plugin Custom
//
//  Created by The Lifu on 10/12/16.
//  Copyright Â© 2016 Abu Group. All rights reserved.
//  Mengubah/menggandakan file ini merupakan pelanggaran hak cipta, tanpa seizin Developer(Annur Alif Ramadhoni/alifu.ramadhoni@gmail.com)


import UIKit

let imageCache = NSCache<NSString, UIImage>()
class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    func loadImageUsingUrlString(_ urlString: String, kata: String, tipe: Int = 0) {
        imageUrlString = urlString
        let url = URL(string: urlString)
        image = nil
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            self.image = imageFromCache
            self.isHidden = false
            return
        }
        if verifyUrl(urlString) == false {
            if tipe == 1 {
//                self.image = #imageLiteral(resourceName: "noimage")
                self.isHidden = false
                return
            } else if tipe == 0 {
//                self.image = #imageLiteral(resourceName: "ic_avatar")
                self.isHidden = false
                return
            }
            self.isHidden = false
            return
        }
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, respones, error) in
            if error != nil {
                print(error as Any)
                if tipe == 1 {
//                    self.image = #imageLiteral(resourceName: "noimage")
                    self.isHidden = false
                    return
                } else if tipe == 0 {
//                    self.image = #imageLiteral(resourceName: "ic_avatar")
                    self.isHidden = false
                    return
                }
            }
            DispatchQueue.main.async(execute: {
                let imageToCache = UIImage(data: data!)
                if imageToCache != nil {
                    if self.imageUrlString == urlString {
                        self.image = imageToCache
                    }
                    imageCache.setObject(imageToCache!, forKey: urlString as NSString)
                } else {
                    if tipe == 1 {
//                        self.image = #imageLiteral(resourceName: "noimage")
                    } else if tipe == 0 {
//                        self.image = #imageLiteral(resourceName: "ic_avatar")
                    } else {
                        
                    }
                    print("Error Loading Image")
                }
                self.isHidden = false
            })
        }).resume()
    }
    
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
}
