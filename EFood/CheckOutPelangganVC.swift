import UIKit

class CheckOutPelangganVC: UIViewController, URLSessionDelegate {
    
    @IBOutlet var headCheck: HeaderApp!
    @IBOutlet var harga: UILabel!
    @IBOutlet var ship: FoodText!
    @IBOutlet var alamat: FoodText!
    @IBOutlet var btnCheck: UIButton!
    
    var dtHarga: String = ""
    var pickerPilih = PilihanVC()
    var arrShip = [[String:AnyObject]]()
    var hrgPilih: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headCheck.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.panggilShip()
        self.ship.tapReg.addTarget(self, action: #selector(self.untukPilih))
        self.ship.addGestureRecognizer(self.ship.tapReg)
        
        self.btnCheck.addTarget(self, action: #selector(self.aksiPesan), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilShip()
    }
    
    func panggilShip() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriPengiriman)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrShip = dt as! [[String : AnyObject]]
                }
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            }
        })
        task.resume()
    }
    
    @objc func untukPilih() {
        pickerPilih = PilihanVC(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)) as PilihanVC
        pickerPilih.btnPilih.addTarget(self, action: #selector(self.hasilPilih), for: .touchUpInside)
        var kumpulan = [[String]]()
        for i in 0..<self.arrShip.count {
            let dt = self.arrShip[i]
            let shipData = dt["ship"] as! String
            let hrgData = "\(dt["harga"] as! Int)"
            kumpulan.append([hrgData, shipData])
        }
        pickerPilih.data = kumpulan
        self.view.addSubview(pickerPilih)
    }
    
    @objc func hasilPilih() {
        let hasil = pickerPilih.getValue()
        self.hrgPilih = hasil[0]
        self.ship.isi.text = hasil[1]
        pickerPilih.closeView()
    }
    
    @objc func aksiPesan() {
        self.panggilPesan()
    }
    
    @objc func gagalCheck() {
        closeLoading()
        self.panggilPesan()
    }
    
    func panggilPesan() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriCartCheckout)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let body = "ship=\(self.ship.isi.text!)&harga_ship=\(self.hrgPilih)&alamat=\(self.alamat.isi.text!)"
        request.httpBody = body.data(using: .utf8)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Sukses Checkout")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
