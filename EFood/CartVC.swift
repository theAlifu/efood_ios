import UIKit

class CartVC: UIViewController, URLSessionDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var headCheckOut: HeaderApp!
    @IBOutlet var collectData: UICollectionView!
    @IBOutlet var btnCheckOut: UIButton!
    
    var arrCast = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let flowLayout = collectData.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectData.register(UINib(nibName: "CartCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.collectData.isHidden = true
        self.panggilCart()

        self.headCheckOut.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.btnCheckOut.addTarget(self, action: #selector(self.panggilCheckOut), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectData.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CartCell
        let data = self.arrCast[indexPath.row]
        cell.nm.text = data["nama_produk"] as? String
        cell.hrg.text = "Rp. \(data["harga"] as! Int)"
        return cell
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilCart()
    }
    
    func panggilCart() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriCart)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrCast = dt as! [[String : AnyObject]]
                }
                self.collectData.reloadData()
                self.collectData.isHidden = false
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            }
        })
        task.resume()
    }
    
    @objc func panggilCheckOut() {
        if DATA_LOGIN.level == "pelanggan" {
            let panggil = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutPelangganVC") as! CheckOutPelangganVC
            self.navigationController?.pushViewController(panggil, animated: true)
        } else if DATA_LOGIN.level == "meja" {
            let panggil = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutMejaVC") as! CheckOutMejaVC
            self.navigationController?.pushViewController(panggil, animated: true)
        }
    }

}
