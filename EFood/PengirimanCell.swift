import UIKit

class PengirimanCell: UICollectionViewCell {
    
    @IBOutlet var ship: UILabel!
    @IBOutlet var harga: UILabel!
    @IBOutlet var btnMore: UIButton!
    @IBOutlet var lebarView: NSLayoutConstraint!
    
    let screenWidth = UIScreen.main.bounds.size.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        lebarView.constant = screenWidth
    }

}
