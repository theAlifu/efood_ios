import UIKit

class KategoriCell: UICollectionViewCell {
    
    @IBOutlet var lebarView: NSLayoutConstraint!
    @IBOutlet var kategori: UILabel!
    @IBOutlet var btnMore: UIButton!
    
    let screenWidth = UIScreen.main.bounds.size.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        lebarView.constant = screenWidth
    }

}
