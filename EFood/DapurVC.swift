import UIKit

class DapurVC: UIViewController, URLSessionDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var headerDapur: HeaderApp!
    @IBOutlet var collectData: UICollectionView!
    
    var arrDapur = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let flowLayout = collectData.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectData.register(UINib(nibName: "DapurCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.collectData.isHidden = true
        self.panggildata()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrDapur.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectData.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DapurCell
        let data = self.arrDapur[indexPath.row]
        cell.tglTransaksi.text = data["tgl_transaksi"] as? String
        cell.status.text = data["track"] as? String
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "DetailDapurVC") as! DetailDapurVC
        panggil.arrDapur = self.arrDapur[indexPath.row]
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggildata()
    }
    
    func panggildata() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriDapur)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrDapur = dt as! [[String : AnyObject]]
                }
                self.collectData.reloadData()
                self.collectData.isHidden = false
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
