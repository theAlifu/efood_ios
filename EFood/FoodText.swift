import UIKit

class FoodText: UIView {
    
    @IBOutlet var judul: UILabel!
    @IBOutlet var isi: UITextField!
    
    var view: UIView!
    let tapReg = UITapGestureRecognizer()
    
    @IBInspectable var titleText: String? {
        get { return judul.text }
        set(titleText) { judul.text = titleText }
    }
    
    @IBInspectable var aktifText: Bool {
        get {
            return self.isi.isEnabled
        }
        set(aktifText) {
            self.isi.isEnabled = aktifText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [ .flexibleHeight, .flexibleWidth]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "FoodText", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

}
