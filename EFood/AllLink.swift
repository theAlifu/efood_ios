import Foundation

let domain = "http://www.efood.develapp.web.id/"

let uriLogin = "\(domain)api/login"
let uriRegister = "\(domain)api/register"

let uriMasterMeja = "\(domain)api/admin/meja"
let uriMasterMejaSimpan = "\(domain)api/admin/meja"
let uriMasterKategori = "\(domain)api/admin/jenis_produk"
let uriMasterKategoriSimpan = "\(domain)api/admin/jenis_produk"
let uriMasterProduk = "\(domain)api/admin/produk"
let uriMasterProdukDetail = "\(domain)api/admin/produk"
let uriMasterProdukSimpan = "\(domain)api/admin/produk"
let uriMasterPengiriman = "\(domain)api/admin/pengiriman"
let uriMasterPengirimanSimpan = "\(domain)api/admin/pengiriman"
let uriMasterUser = "\(domain)api/admin/jenis_produk"
let uriMasterUserSimpan = "\(domain)api/admin/jenis_produk"


let uriOrderProduk = "\(domain)api/produk"
let uriOrderProdukDetail = "\(domain)api/produk"
let uriCart = "\(domain)api/cart"
let uriCartPesan = "\(domain)api/cart"
let uriCartCheckout = "\(domain)api/cart/checkout"
let uriPengiriman = "\(domain)api/pengiriman"

let uriDapur = "\(domain)api/dapur"
let uriDapurProses = "\(domain)api/dapur"

let uriKasir = "\(domain)api/kasir"
let uriKasirBayar = "\(domain)api/kasir"
