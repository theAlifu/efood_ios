import UIKit

class DetailProdukVC: UIViewController {
    
    @IBOutlet var headerDetail: HeaderApp!
    @IBOutlet var img: CustomImageView!
    @IBOutlet var nama: UILabel!
    @IBOutlet var kategori: UILabel!
    @IBOutlet var Harga: UILabel!
    @IBOutlet var keterangan: UILabel!
    
    var dtImg: String = ""
    var dtNama: String = ""
    var dtKategori: String = ""
    var dtHarga: String = ""
    var dtKeterangan: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerDetail.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.img.loadImageUsingUrlString(self.dtImg, kata: "")
        self.nama.text = self.dtNama
        self.kategori.text = self.dtKategori
        self.Harga.text = "Rp." + self.dtHarga
        self.keterangan.text = self.dtKeterangan
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }

}
