import UIKit

class MejaCell: UICollectionViewCell {
    
    @IBOutlet var lebarView: NSLayoutConstraint!
    @IBOutlet var meja: UILabel!
    @IBOutlet var kapasitas: UILabel!
    @IBOutlet var btnMore: UIButton!
    
    let screenWidth = UIScreen.main.bounds.size.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        lebarView.constant = screenWidth
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
    }

}
