//
//  PilihanVC.swift
//  TourismApp
//
//  Created by Alifu Poenya on 2/14/17.
//  Copyright © 2017 Alifu Poenya. All rights reserved.
//

import UIKit

class PilihanVC: UIView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet var viewBack: UIView!
    @IBOutlet var pilihPilih: UIPickerView!
    @IBOutlet var btnBatal: UIButton!
    @IBOutlet var btnPilih: UIButton!
    
    var myView: UIView!
    var data = [[String]]()
    var returnValue = [String]()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        myView = loadViewFromNib()
        myView.frame = bounds
        myView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        addSubview(myView)
        viewBack.layer.cornerRadius = 5
        viewBack.layer.borderColor = UIColor.black.cgColor
        viewBack.layer.borderWidth = 1
        pilihPilih.delegate = self
        pilihPilih.dataSource = self
        btnBatal.addTarget(self, action: #selector(self.closeView), for: .touchUpInside)
        btnBatal.setTitle("Batal", for: .normal)
        btnPilih.setTitle("Pilih", for: .normal)
        btnBatal.layer.cornerRadius = 5
        btnPilih.layer.cornerRadius = 5
        self.viewBack.layoutIfNeeded()
        let c = viewBack.frame.origin.y
        viewBack.frame.origin.y = self.frame.size.height
        viewBack.isHidden = false
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2, options: UIViewAnimationOptions(), animations: {
            self.viewBack.frame.origin.y = c
            self.alpha = 1
        }, completion: nil)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "PilihanVC", bundle: bundle)
        let myView = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return myView
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row][1]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        returnValue = data[row]
    }
    
    func getValue() -> [String] {
        returnValue = data[pilihPilih.selectedRow(inComponent: 0)]
        return returnValue
    }
    
    @objc func closeView() {
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2, options: UIViewAnimationOptions(), animations: {
            self.viewBack.frame.origin.y = self.frame.size.height
            self.alpha = 0
        }) { (Bool) in
            self.removeFromSuperview()
        }
    }
    
}
