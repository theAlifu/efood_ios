import UIKit

class MasterProductVC: UIViewController, URLSessionDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var headProduk: HeaderApp!
    @IBOutlet var collectData: UICollectionView!
    
    var arrProduk = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let flowLayout = collectData.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectData.register(UINib(nibName: "ProdukCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.headProduk.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.headProduk.btnDua.addTarget(self, action: #selector(self.panggilForm), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.panggilProduk()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func panggilForm() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "FormKategoriVC") as! FormKategoriVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrProduk.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectData.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProdukCell
        let data = self.arrProduk[indexPath.row]
        let urlImg = data["img_head"] as! String
        cell.img.loadImageUsingUrlString(urlImg, kata: "")
        cell.nama.text = data["nama"] as? String
        cell.kategori.text = data["jenis"] as? String
        cell.hrg.text = "Rp. \(data["harga"] as! Int)"
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.panggilOpsiList(sender:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.arrProduk[indexPath.row]
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "DetailProdukVC") as! DetailProdukVC
        panggil.dtImg = data["img_head"] as! String
        panggil.dtNama = data["nama"] as! String
        panggil.dtKategori = data["jenis"] as! String
        panggil.dtKeterangan = data["ket"] as! String
        panggil.dtHarga = "\(data["harga"] as! Int)"
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    @objc func panggilOpsiList(sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Pilih", preferredStyle: .alert)
        let edit = UIAlertAction(title: "Edit", style: .default) { (edit) in
            
        }
        let hapus = UIAlertAction(title: "Hapus", style: .default) { (hapus) in
            
        }
        let batal = UIAlertAction(title: "Batal", style: .cancel) { (hapus) in
            
        }
        alert.addAction(edit)
        alert.addAction(hapus)
        alert.addAction(batal)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilProduk()
    }
    
    func panggilProduk() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriMasterProduk)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrProduk = dt as! [[String : AnyObject]]
                }
                self.collectData.reloadData()
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
