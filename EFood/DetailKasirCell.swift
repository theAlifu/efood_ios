import UIKit

class DetailKasirCell: UICollectionViewCell {

    @IBOutlet var nama: UILabel!
    @IBOutlet var hrg: UILabel!
    @IBOutlet var lebarView: NSLayoutConstraint!
    
    let screenWidth = UIScreen.main.bounds.size.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        lebarView.constant = screenWidth
    }

}
