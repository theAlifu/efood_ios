import UIKit

class SettingVC: UIViewController {
    
    var kirimData: SettingTVC!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "view1" {
            let dt = segue.destination as! SettingTVC
            dt.tempatData = self
            self.kirimData = dt
        }
    }

}
