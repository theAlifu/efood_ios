import UIKit

class HeaderApp: UIView {

    @IBOutlet var btnSatu: UIButton!
    @IBOutlet var btnDua: UIButton!
    @IBOutlet var judul: UILabel!
    
    var view: UIView!
    
    @IBInspectable var titleText: String? {
        get { return judul.text }
        set(titleText) { judul.text = titleText }
    }
    
    @IBInspectable var tombolSatu: UIImage {
        get {
            var img = UIImage()
            if let _ = btnSatu.imageView?.image {
                img = (btnSatu.imageView?.image)!
            }
            return img
        }
        
        set (tombolSatu) {
            self.btnSatu.setImage(tombolSatu, for: UIControlState())
            self.btnSatu.setImage(tombolSatu, for: .selected)
        }
    }
    
    @IBInspectable var tombolDua: UIImage {
        get {
            var img = UIImage()
            if let _ = btnDua.imageView?.image {
                img = (btnDua.imageView?.image)!
            }
            return img
        }
        
        set (tombolDua) {
            self.btnDua.setImage(tombolDua, for: UIControlState())
            self.btnDua.setImage(tombolDua, for: .selected)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [ .flexibleHeight, .flexibleWidth]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "HeaderApp", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

}
