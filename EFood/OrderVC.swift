import UIKit

class OrderVC: UIViewController, URLSessionDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet var collectOrder: UICollectionView!
    @IBOutlet var headOrder: HeaderApp!
    
    var arrKategori = [[String:AnyObject]]()
    var arrFeed = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let flowLayout = collectOrder.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectOrder.register(UINib(nibName: "FeedCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.collectOrder.isHidden = true
        self.panggilProduk()
        
        self.headOrder.btnDua.addTarget(self, action: #selector(self.panggilCart), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectOrder.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeedCell
        let data = self.arrFeed[indexPath.row]
        let urlImg = data["img_head"] as! String
        cell.backImg.loadImageUsingUrlString(urlImg, kata: "")
        cell.nama.text = data["nama"] as? String
        cell.kategori.text = data["jenis"] as? String
        cell.hrg.text = "Rp. \(data["harga"] as! Int)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.arrFeed[indexPath.row]
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "DetailProdukOrderVC") as! DetailProdukOrderVC
        panggil.dtImg = data["img_head"] as! String
        panggil.dtNama = data["nama"] as! String
        panggil.dtKategori = data["jenis"] as! String
        panggil.dtKeterangan = data["ket"] as! String
        panggil.dtSatuan = data["satuan"] as! String
        panggil.dtHarga = "\(data["harga"] as! Int)"
        panggil.dtId = "\(data["id"] as! Int)"
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilProduk()
    }
    
    func panggilProduk() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriOrderProduk)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrFeed = dt as! [[String : AnyObject]]
                }
                self.collectOrder.reloadData()
                self.collectOrder.isHidden = false
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
            }
        })
        task.resume()
    }
    
    @objc func panggilCart() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }

}
