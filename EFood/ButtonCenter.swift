import UIKit

class ButtonCenter: UIView {
    
    @IBOutlet var img: UIImageView!
    @IBOutlet var judul: UILabel!
    
    var view: UIView!
    var tapReg = UITapGestureRecognizer()
    
    @IBInspectable var titleText: String? {
        get { return judul.text }
        set(titleText) { judul.text = titleText }
    }
    
    @IBInspectable var imgTitle: UIImage? {
        get {
            return self.img.image!
        }
        set(imgTitle) {
            self.img.image = imgTitle
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [ .flexibleHeight, .flexibleWidth]
        addSubview(view)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "ButtonCenter", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
