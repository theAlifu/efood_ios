import UIKit

class MasterKategoriVC: UIViewController, URLSessionDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var headKategori: HeaderApp!
    @IBOutlet var collectData: UICollectionView!
    
    var arrKategori = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let flowLayout = collectData.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectData.register(UINib(nibName: "KategoriCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.headKategori.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.headKategori.btnDua.addTarget(self, action: #selector(self.panggilForm), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.panggilKategori()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func panggilForm() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "FormKategoriVC") as! FormKategoriVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrKategori.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectData.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! KategoriCell
        let data = self.arrKategori[indexPath.row]
        cell.kategori.text = "\(data["jenis"] as! String)"
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.panggilOpsiList(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func panggilOpsiList(sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Pilih", preferredStyle: .alert)
        let edit = UIAlertAction(title: "Edit", style: .default) { (edit) in
            
        }
        let hapus = UIAlertAction(title: "Hapus", style: .default) { (hapus) in
            
        }
        let batal = UIAlertAction(title: "Batal", style: .cancel) { (hapus) in
            
        }
        alert.addAction(edit)
        alert.addAction(hapus)
        alert.addAction(batal)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilKategori()
    }
    
    func panggilKategori() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriMasterKategori)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrKategori = dt as! [[String : AnyObject]]
                }
                self.collectData.reloadData()
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
