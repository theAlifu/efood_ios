import UIKit

class RegisVC: UIViewController, URLSessionDelegate {
    
    @IBOutlet var headRegis: HeaderApp!
    @IBOutlet var email: FoodText!
    @IBOutlet var nama: FoodText!
    @IBOutlet var noTelp: FoodText!
    @IBOutlet var pass: FoodText!
    @IBOutlet var passLagi: FoodText!
    @IBOutlet var btnDaftar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headRegis.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.pass.isi.isSecureTextEntry = true
        self.passLagi.isi.isSecureTextEntry = true
        self.btnDaftar.addTarget(self, action: #selector(self.akdiDaftar), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func akdiDaftar() {
        self.panggilRegis()
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilRegis()
    }
    
    func panggilRegis() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriRegister)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        let body = "email=\(self.email.isi.text!)&password=\(self.pass.isi.text!)&c_password=\(self.passLagi.isi.text!)&name=\(self.nama.isi.text!)&level=pelanggan&telp=\(self.noTelp.isi.text!)"
        request.httpBody = body.data(using: .utf8)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Tersimpan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
