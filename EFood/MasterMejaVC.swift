import UIKit

class MasterMejaVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, URLSessionDelegate {
    
    @IBOutlet var headMeja: HeaderApp!
    @IBOutlet var collectMeja: UICollectionView!
    
    var arrMeja = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let flowLayout = collectMeja.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectMeja.register(UINib(nibName: "MejaCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.headMeja.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.headMeja.btnDua.addTarget(self, action: #selector(self.panggilFormMeja), for: .touchUpInside)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.panggilLMeja()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMeja.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectMeja.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MejaCell
        let data = self.arrMeja[indexPath.row]
        cell.meja.text = "Meja: \(data["no_meja"] as! String)"
        cell.kapasitas.text = "Kapasitas: \(data["kapasitas"] as! Int)"
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.panggilOpsiList(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func panggilOpsiList(sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Pilih", preferredStyle: .alert)
        let edit = UIAlertAction(title: "Edit", style: .default) { (edit) in
            
        }
        let hapus = UIAlertAction(title: "Hapus", style: .default) { (hapus) in
            
        }
        let batal = UIAlertAction(title: "Batal", style: .cancel) { (hapus) in
            
        }
        alert.addAction(edit)
        alert.addAction(hapus)
        alert.addAction(batal)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilLMeja()
    }
    
    func panggilLMeja() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriMasterMeja)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let dt = swiftyJsonVar["response"]["message"].arrayObject {
                    self.arrMeja = dt as! [[String : AnyObject]]
                }
                self.collectMeja.reloadData()
                closeLoading()
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }
    
    @objc func panggilFormMeja() {
        let panggil = self.storyboard?.instantiateViewController(withIdentifier: "FormMejaVC") as! FormMejaVC
        self.navigationController?.pushViewController(panggil, animated: true)
    }
    
}
