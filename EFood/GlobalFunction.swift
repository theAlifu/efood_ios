import Foundation
import UIKit

// untuk proses loading
var loadWindow = LoadingPage()
func panggilLoading(sender: UIViewController) {
    loadWindow = LoadingPage(frame: CGRect(x: 0, y: 0, width: sender.view.bounds.width, height: sender.view.bounds.height))
    loadWindow.btnInfo.isHidden = true
    loadWindow.info.isHidden = true
    loadWindow.putar.isHidden = false
    sender.view.addSubview(loadWindow)
}

func stopLoading(untukBtn: String = "Muat Ulang", untukPesan: String = "") {
    loadWindow.btnInfo.isHidden = false
    loadWindow.info.isHidden = false
    loadWindow.putar.isHidden = true
    loadWindow.btnInfo.setTitle(untukBtn, for: .normal)
    loadWindow.info.text = untukPesan
}

func closeLoading() {
    loadWindow.btnInfo.removeTarget(nil, action: nil, for: .allEvents)
    loadWindow.removeFromSuperview()
}
// ------
