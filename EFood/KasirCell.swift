import UIKit

class KasirCell: UICollectionViewCell {

    @IBOutlet var tglTransaksi: UILabel!
    @IBOutlet var status: UILabel!
    @IBOutlet var lebarView: NSLayoutConstraint!
    
    let screenWidth = UIScreen.main.bounds.size.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        lebarView.constant = screenWidth
    }

}
