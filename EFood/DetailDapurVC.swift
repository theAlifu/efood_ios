import UIKit

class DetailDapurVC: UIViewController, URLSessionDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet var headerDetail: HeaderApp!
    @IBOutlet var tglTransaksi: UILabel!
    @IBOutlet var collectData: UICollectionView!
    @IBOutlet var btnProses: UIButton!
    
    var arrDapur = [String:AnyObject]()
    var arrPesanan = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let flowLayout = collectData.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        self.collectData.register(UINib(nibName: "DetailDapurCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        self.headerDetail.btnSatu.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
        self.arrPesanan = arrDapur["transaksi_detail"] as! [[String:AnyObject]]
        self.tglTransaksi.text = "Tanggal Transaksi: \(arrDapur["tgl_transaksi"] as! String)"
        self.collectData.reloadData()
        
        self.btnProses.addTarget(self, action: #selector(self.aksiProses), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func kembali() {
        self.navigationController?.popViewController(animated: true)
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPesanan.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dt = self.arrPesanan[indexPath.row]
        let cell = self.collectData.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DetailDapurCell
        cell.nama.text = dt["nama"] as? String
        cell.hrg.text = "\(dt["harga"] as! Int)"
        return cell
    }
    
    @objc func aksiProses() {
        self.panggilProses()
    }
    
    @objc func gagal() {
        closeLoading()
        self.panggilProses()
    }
    
    @objc func tutup() {
        closeLoading()
    }
    
    func panggilProses() {
        panggilLoading(sender: self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = URL(string: uriDapurProses + "/\(self.arrDapur["id"] as! Int)")
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "PUT"
        request.addValue("Bearer " + DATA_LOGIN.token, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                stopLoading(untukBtn: "Muat Ulang", untukPesan: "Gagal Terhubung ke Server")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.gagal), for: .touchUpInside)
                return
            }
            let swiftyJsonVar = JSON(data: data!)
            let status = swiftyJsonVar["diagnostics"]["status"].int
            print(swiftyJsonVar)
            if status == 200 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Sukses")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.kembali), for: .touchUpInside)
            } else if status == 401 {
                let msg = swiftyJsonVar["response"]["message"].string
                stopLoading(untukBtn: "Tutup", untukPesan: msg!)
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                stopLoading(untukBtn: "Tutup", untukPesan: "Terjadi Kesalahan")
                loadWindow.btnInfo.addTarget(self, action: #selector(self.tutup), for: .touchUpInside)
            }
        })
        task.resume()
    }

}
