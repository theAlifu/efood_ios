import UIKit

class FirstRun: UIViewController {
    
    var mainNavigationController: UINavigationController!
    var mainViewController: LoginVC!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.chooseViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func chooseViewController() {
        self.initiateMainVC()
    }
    
    @objc func skipButtonTapped() {
        self.initiateMainVC()
    }
    
    func initiateMainVC() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            for v in self.view.subviews {
                v.superview!.alpha = 0
                v.isHidden = false
                UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                    v.superview!.alpha = 1
                    v.isHidden = true
                    v.removeFromSuperview()
                }, completion: nil)
            }
        }, completion: nil)
        self.mainViewController = UIStoryboard.mainViewController()
        self.mainViewController.view.bounds = self.view.bounds
        self.mainNavigationController = UINavigationController(rootViewController: self.mainViewController)
        self.view.addSubview(self.mainNavigationController.view)
        self.addChildViewController(self.mainNavigationController)
        self.mainNavigationController.didMove(toParentViewController: self)
        mainNavigationController.isNavigationBarHidden = true
    }

}

extension UIStoryboard {
    
    class func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func mainViewController() -> LoginVC? {
        return mainStoryboard().instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
    }
    
}

